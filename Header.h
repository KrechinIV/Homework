#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <ctime>
#include <sys/time.h>

using namespace std;

struct Data
{
    int n, nthreads;
    double *A,*X;
    int number;
    int *column;
    int *mark;
    int *var;
    double *time;
};

int Get_Matrix(int n, double* A, FILE* fin);
int Inverse(Data *data);
void Output(double *A, int n, int m);
void Residual(double* A, double* X, int n);
void synchronize(int total_threads);
void *work(void *d);
double get_full_time(void);
