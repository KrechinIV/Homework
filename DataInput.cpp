#include "Header.h"

double f(int x, int y);

int Get_Matrix(int n, double* A, FILE* fin)
{
    int i, j, n1, in;

	if (fin)
	{
        fscanf(fin, "%d", &n1);
        if (n1 != n)
        {
            std::cout << "Incorrect size\n";
            return -1;
        }
		for (i = 0; i<n; i++)
			for (j = 0; j<n; j++)
			{
				if ((feof(fin)) || (fscanf(fin, "%lf", &A[i*n + j]) != 1))
					return -1;
			}
	}
	else
	{
        for (i = 0; i < n; i++)
        {
            in = i*n;
            for (j = 0; j < n; j++)
			{
                A[in + j] = f(i, j);
			}
        }
	}
	return 0;
}

double f(int x, int y)
{
	return fabs(x-y);
//	return x+y;
//	return 1.0/(x+y+1.0);
}

