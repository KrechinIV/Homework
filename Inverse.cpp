#include "Header.h"

int Inverse(Data *data) 
{
    int i, j, k;
    double ratio, max;

    int StartRow, FinishRow;
    int StartCol, FinishCol;
    
    int nthreads = data->nthreads;
    int n = data->n;
    
    double eps = 1e-20;
    
    if (data->number == 0)
    {
        for (i = 0; i < n; i++)
        {
            data->var[i] = i;
            data->column[i] = i;
            for (j = 0; j < n; j++)
            {
                if (i == j) data->X[i*n+j] = 1;
                else data->X[i*n+j] = 0;
            }
        }
    }
    
    for (i = 0; i < n; i++)
    {        
		if (data->number == 0)
        {
			max = data->A[i * n + i];
			data->column[i] = i;
			for (j = i + 1; j < n; j++)
			{
				if (fabs(data->A[i*n + j]) > max)
				{
					max = fabs(data->A[i*n + j]);
					data->column[i] = j;
				}
			}
			
			if (fabs(max) < eps) data->mark[0] = 1;
            else
            {
                if (data->column[i] != i) 
                {
                    swap(data->var[i], data->var[data->column[i]]);
                    
                    for (j = 0; j < n; j++)
                    {
                    	swap(data->A[j*n + data->column[i]], data->A[j*n+i]);
					}
                }
                
                ratio = 1.0/(data->A[i*n + i]);
                
                for (j = i; j < n; j++)
                {
                	data->A[i*n + j] *= ratio;
				}
                    
                for (j = 0; j < n; j++)
                {
                	data->X[i*n + j] *= ratio;
				}
            }
        }
    
        synchronize(nthreads);
        
        if(data->mark[0] == 1) return -1;
        
        StartRow = (n - i - 1) * data->number/nthreads + i + 1;
        FinishRow = (n - i - 1) * (data->number + 1)/nthreads + i + 1;
        
        for (j = StartRow; j < FinishRow; j++)
        {
            ratio = data->A[j*n + i];
            
            for (k = i; k < n; k++)
            {
            	data->A[j*n+k] -= data->A[i*n+k] * ratio;
			}
            
            for (k = 0; k < n; k++)
            {
            	data->X[j*n+k] -= data->X[i*n+k] * ratio;
			}
        }
    }
    
    synchronize(nthreads);
    
    StartCol = n * data->number/nthreads;
    FinishCol = n * (data->number + 1)/nthreads;
    
    for (k = StartCol; k < FinishCol; k++)
    {
        for (i = n-1; i >= 0; --i)
        {
            ratio = data->X[i*n + k];
            
            for (j = i + 1; j < n; j++)
            {
            	ratio -= data->A[i*n+j] * data->X[j*n+k];
			}
            
            data->X[i*n+k] = ratio;
        }
    }
    
    synchronize(nthreads);
    
    if (data->number == 0)
    {
        for (i = 0; i < n; i++)
        {
        	for (j = 0; j < n; j++)
        	{
        		data->A[data->var[i] * n + j] = data->X[i * n + j];
			}
		}
            
        for (i = 0; i < n; ++i)
        {
        	for (j = 0; j < n; ++j)
        	{
        		data->X[i * n + j] = data->A[i * n + j];
			}
		}   
    }

    return 0;
}



