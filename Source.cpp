#include "Header.h"

using namespace std;

int main(int argc, char **argv)
{
	int m, n, i, method;
	double *A;
	double *X;
	double *X1;
	int *column;
	int *mark;
	int *var;
	double *time;
	pthread_t *threads;
	int nthreads;

    FILE *fin = NULL;
    
    if (argc < 4)
	{
		cout << "Usage: ./prog <n> <m> <nthreads>" << endl;
		return -1;
	}
	
	m = atoi(argv[2]);
	nthreads = atoi(argv[3]);
	
	if (sscanf(argv[1], "%d", &n) != 1)
	{
		fin = fopen("input.txt", "r");
		if (fin == NULL)
		{
			cout << "FILE ERROR" << endl;
			return -1;
		}
		else fscanf(fin, "%d", &n);
	}

	if (n < 1)
	{
		cout << "Incorrect size!" << endl;
		return -1;
	}

    A = new double[n*n];
	X = new double[n*n];
	X1 = new double[n*n];
	time = new double[nthreads];
	column = new int[n];
	mark = new int[nthreads];
	Data *data = new Data[nthreads];
    threads = new pthread_t[nthreads];
	var = new int[n];
	
	if (method == 1)
	{
        fin = fopen("input.txt", "r");
		if (fin == NULL)
		{
			cout << "FILE ERROR\n";
            delete[]X1;
            delete[]X;
            delete[]A;
            delete[]time;
            delete[]data;
            delete[]column;
            delete[]mark;
            delete[]threads;
            delete[]var;
			return -1;
		}
	}

	if (Get_Matrix(n, A, fin) != 0)
	{
        delete[]X1;
        delete[]X;
        delete[]A;
        delete[]time;
        delete[]mark;
        delete[]data;
        delete[]column;
        delete[]threads;
        delete[]var;
        if (method == 1) fclose(fin);
		return -1;
	}

	for (i = 0; i < n*n; i++)
	{
		X1[i] = A[i];
	}

	cout << "Entered matrix: \n";
	Output(A, n, m);
	
	for (i = 0; i < nthreads; i++)
	{
		data[i].n = n;
		data[i].A = A;
		data[i].X = X;
		data[i].number = i;
		data[i].nthreads = nthreads;
		data[i].column= column;
		data[i].mark = mark;
		data[i].var = var;
		data[i].time = time;
	}

	for (i = 0; i < nthreads; i++)
	{
		if (pthread_create(threads + i, 0, work, data + i))
		{
			cout << "Error in pthread_create!\n";
			delete[]X1;
        	delete[]X;
        	delete[]A;
        	delete[]time;
        	delete[]data;
        	delete[]mark;
        	delete[]column;
            delete[]threads;
            delete[]var;
        	if (method == 1) fclose(fin);
			return -1;
		}
	}
		
	for (i = 0; i < nthreads; i++)
	{
		if (pthread_join(threads[i], 0))
		{
			cout << "Error in pthread_join!\n";

			delete[]X1;
        	delete[]X;
        	delete[]A;
        	delete[]time;
        	delete[]data;
        	delete[]mark;
        	delete[]column;
            delete[]threads;
            delete[]var;
        	if (method == 1) fclose(fin);
			return -1;
		}
	}
	
	if(data[0].mark[0] == 1)
	{
		cout << "Degenerate matrix!\n";
		delete[]X1;
        delete[]X;
        delete[]A;
        delete[]time;
        delete[]data;
        delete[]mark;
        delete[]column;
        delete[]threads;
        delete[]var;
        if (method == 1) fclose(fin);
		return -1;
	}
	
	cout << "\nInverse matrix: \n";
	Output(X, n, m);
	
	cout << "Runtime: " << data[0].time[0] << endl;
	
//	Residual(X1, X, n);

	delete[]X1;
    delete[]X;
    delete[]A;
    delete[]time;
    delete[]data;
    delete[]mark;
    delete[]column;
    delete[]threads;
    delete[]var;
    if (method == 1) fclose(fin);
	return 0;
}

