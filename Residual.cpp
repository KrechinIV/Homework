#include "Header.h"

using namespace std;

void Residual(double* A, double* X, int n)
{
	int i;
	int j;
	int k;
	double a;
	double sum = 0.0;
	double maxi = 0.0;
	for (i = 0; i<n; ++i)
	{
		sum = 0.0;
		for (j = 0; j<n; ++j)
		{
			a = 0.0;
			for (k = 0; k<n; ++k)
				a += A[i*n + k] * X[k*n + j];
			if (i == j)
				a -= 1.0;
			sum = fabs(a);
		}
		if (sum > maxi)
			maxi = sum;
	}
	cout << "\nResidual: " << maxi << "\n";
}

