#include "Header.h"

void *work(void *d)
{
    Data *data=(Data*)d;
    synchronize(data->nthreads);
    if (data->number==0)
    {
        data->time[0]=get_full_time();
    }
    Inverse(data);
    synchronize(data->nthreads);
    if (data->number==0)
    {
         data->time[0]=get_full_time()-data->time[0];
    }
    return 0;
}

void synchronize(int total_threads)
{
	static pthread_mutex_t mutex=PTHREAD_MUTEX_INITIALIZER;
	static pthread_cond_t condvar_in=PTHREAD_COND_INITIALIZER;
	static pthread_cond_t condvar_out=PTHREAD_COND_INITIALIZER;
	static int threads_in=0;
	static int threads_out=0;
	pthread_mutex_lock(&mutex);
	threads_in++;
	if (threads_in>=total_threads)
	{
		threads_out=0;
		pthread_cond_broadcast(&condvar_in);
	}
	else
	{
		while (threads_in<total_threads)
		{
			pthread_cond_wait(&condvar_in, &mutex);
		}
	}
	threads_out++;
	if (threads_out>=total_threads)
	{
		threads_in=0;
		pthread_cond_broadcast(&condvar_out);
	}
	else
	{
		while (threads_out<total_threads)
		{
			pthread_cond_wait(&condvar_out, &mutex);
		}
	}
	pthread_mutex_unlock(&mutex);
}

double get_full_time(void)
{
	struct timeval t;

	gettimeofday(&t, 0);

	return t.tv_sec+t.tv_usec/1000000.0;
}



